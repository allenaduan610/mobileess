import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

final String tableName = 'devicetable';
final int personnelID = personnelID;
final String mobileNumber = mobileNumber;
final String deviceDetail = deviceDetail;
final String birthDate = birthDate;
final String personnelName = personnelName;
final String notificationToken = notificationToken;
final String employmentStatus = employmentStatus;

class DeviceTable{
  int personnelID;
  final String mobileNumber;
  final String deviceDetail;
  final String birthDate;
  final String personnelName;
  final String notificationToken;
  final String employmentStatus;

  DeviceTable({
     this.personnelID,
     this.mobileNumber,
     this.deviceDetail,
     this.birthDate,
     this.personnelName,
     this. notificationToken,
     this.employmentStatus
    });

    Map<String, dynamic> toMap(){
      return {
        // personnelID : this.personnelID,
        mobileNumber : this.mobileNumber,
        deviceDetail : this.deviceDetail,
        birthDate : this.birthDate,
        personnelName : this.personnelName,
        notificationToken : this.notificationToken,
        employmentStatus : this.employmentStatus
      };
    }
}

class TodoHelper{
  Database db;

  TodoHelper(){
    initDatabase();
  }

  Future<void> initDatabase() async{
    db = await openDatabase(
      join(await getDatabasesPath(), 'mobile_ess.db'),
      onCreate: (db, version){
        return db.execute("CREATE TABLE $tableName($personnelID INTEGER PRIMARY KEY AUTOINCREMENT, $mobileNumber TEXT NOT NULL,)");
        // $deviceDetail TEXT NOT NULL, $birthDate TEXT NOT NULL, $personnelName TEXT NOT NULL, $notificationToken TEXT NOT NULL, $employmentStatus TEXT NOT NULL)");
      },
      version: 1
    );
  }

  Future<void> insertTask(DeviceTable task) async{
    try{
      db.insert(tableName, task.toMap(), conflictAlgorithm: ConflictAlgorithm.replace);
    }catch(_){
      print(_);
    }
  }
  
  Future<List<DeviceTable>> getAllTask() async{
    final List<Map<String, dynamic>> tasks = await db.query(tableName);

    return List.generate(tasks.length, (i){
      return DeviceTable(mobileNumber: tasks[i][mobileNumber], personnelID: tasks[i][personnelID],);
      //  deviceDetail: tasks[i][deviceDetail], birthDate: tasks[i][birthDate], personnelName: tasks[i][personnelName], notificationToken: tasks[i][notificationToken],
      //   employmentStatus: tasks[i][employmentStatus]);
    });
  }
}