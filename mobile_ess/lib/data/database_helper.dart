import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DeviceTable {

    static final _dbName = 'mobile_ess.db';
    static final _dbVersion = 1;
    static final _tableName = 'deviceTable';

    static final personnelID = '_personnelID';
    static final mobileNumber = 'mobileNumber';
    static final deviceDetail = '_deviceDetail';
    static final birthDate = 'birthDate';
    static final personnelName = 'personnelName';
    static final notificationToken = 'notificationToken';
    static final employmentStatus = 'employmentStatus';


  
    DeviceTable._privateConstructor();
    static final DeviceTable instance = DeviceTable._privateConstructor();

    static Database _database;
    Future<Database> get database async{
      if(_database!=null) return _database;

      _database = await _initiateDatabase();
      return _database;
    }

    _initiateDatabase() async {
      Directory directory = await getApplicationDocumentsDirectory();
      String path = join(directory.path,_dbName);
      return await openDatabase(path,version: _dbVersion,onCreate: _onCreate);


    }

    Future _onCreate(Database db, int version){
      db.query(
        '''
          CREATE TABLE $_tableName(
            $personnelID INTEGER PRIMARY KEY,
            $mobileNumber INTEGER NOT NULL,
            $deviceDetail TEXT NOT NULL,
            $birthDate TEXT NOT NULL,
            $personnelName TEXT NOT NULL,
            $notificationToken TEXT NOT NULL,
            $employmentStatus TEXT NOT NULL )
        '''
      );
    }



    Future<int> insert(Map<String, dynamic> row) async {
      Database db = await instance.database;
      return await db.insert(_tableName, row);
    }

    Future<List<Map<String, dynamic>>> queryAll() async{
      Database db = await instance.database;
      return await db.query(_tableName);
    }

    Future<int> update(Map<String, dynamic> row) async{
      Database db = await instance.database;
      int id = row[personnelID];
      return await db.update(_tableName, row, where: '$personnelID = ?', whereArgs: [id]);
    }

    Future<int> delete(int id) async {
      Database db = await instance.database;
      return await db.delete(_tableName, where: '$personnelID = ?', whereArgs: [id]);
    }

}