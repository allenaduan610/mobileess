import 'dart:async';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ConnectApi extends StatefulWidget {
  @override
  _ConnectApiState createState() => _ConnectApiState();
}

class _ConnectApiState extends State<ConnectApi> {
  Future<String> getData() async {
    http.Response response = await http.get(
        Uri.encodeFull("34.87.153.218,1437"),
        headers: {"Accept": "application/json"});

    print(response.body);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: RaisedButton(child: Text("Get Data"), onPressed: getData),
      ),
    );
  }
}
