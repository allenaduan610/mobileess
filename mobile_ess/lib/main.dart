import 'package:flutter/material.dart';
import 'package:connectivity/connectivity.dart';
import 'package:mobile_ess/data/connect_api.dart';
import 'package:mobile_ess/present/appmenu.dart';
import 'package:mobile_ess/present/systemUI/registration.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mobile ESS',
      home: Notifications_(),
    );
  }
}

class InternetValidation extends StatefulWidget {
  @override
  _InternetValidationState createState() => _InternetValidationState();
}

class _InternetValidationState extends State<InternetValidation> {
  String result = '';
  var Colorsval = Colors.white;

  @override
  void initState() {
    CheckStatus();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorsval,
      body: Container(alignment: Alignment.center,
      child: Text(result!=null?result:'Unknown',style: TextStyle(color: Colors.white, fontWeight: FontWeight.w700, fontSize: 25),),
      ),
    );
  }


void CheckStatus() {
  Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if(result==ConnectivityResult.mobile || result==ConnectivityResult.wifi){
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => RegistrationScreen()));
      }else{
         Navigator.of(context).push(MaterialPageRoute(builder: (context) => InternetValidation()));
        // ChangeValues('No Internet', Colors.red);
      }
   });
}

void ChangeValues(String resultval, Color colorval){
  setState((){
    result = resultval;
    Colorsval = colorval;
  });
}
}
