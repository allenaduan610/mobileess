import 'dart:async';

import 'package:connectivity/connectivity.dart';

class InternetValidation {

  StreamController<ConnectivityResult> connectionStatusController =
    StreamController<ConnectivityResult>();
  
  InternetValidation() {

    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      connectionStatusController.add(result);
     });
  }
}