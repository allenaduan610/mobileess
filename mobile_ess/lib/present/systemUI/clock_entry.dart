import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ClockEntry extends StatefulWidget {
  @override
  _ClockEntryState createState() => _ClockEntryState();
}

class _ClockEntryState extends State<ClockEntry> {
  String _timeString;
  // bool val = false;
  // onSwitchValueChanged(bool newVal) {
  //   setState(() {
  //     val = newVal;
  //   });
  // }

  

  @override
  void initState() {
    _timeString = _formatDateTime(DateTime.now());
    Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
    super.initState();
  }

  void _getTime() {
  final DateTime now = DateTime.now();
  final String formattedDateTime = _formatDateTime(now);
  setState((){
    _timeString = formattedDateTime;
  });
}

String _formatDateTime(DateTime dateTime) {
  return DateFormat('MM/dd/yyyy hh:mm:ss').format(dateTime);
}
  

  @override
  Widget build(BuildContext context) {
    return Container(
      // appBar: AppBar(
      //   title: Text('Clock Entry'),
      //   backgroundColor: Colors.red[900],
      //   automaticallyImplyLeading: true,
      //   leading: IconButton(
      //     icon: Icon(Icons.arrow_back),
      //     onPressed: () {},
      //   ),
      // ),
      // body: 
      child: Column(
        children: <Widget>[
          Center(
            child: Padding(
                padding: EdgeInsets.fromLTRB(10, 160, 10, 10),
                // child: Text(
                //   // _timeString,
                //   style: TextStyle(fontSize: 19),
                // )
                ),
          ),
          Text(
            
            _timeString,
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(50, 10, 10, 10),
            child: Row(
              children: <Widget>[
                RaisedButton(
                  padding: EdgeInsets.all(15),
                  child: Text('Time-In', style: TextStyle(color: Colors.white),),
                  color: Colors.grey,
                  onPressed: () {},
                ),
                Padding(padding: EdgeInsets.fromLTRB(40, 10, 40, 40)),
                RaisedButton(
                  padding: EdgeInsets.all(15),
                  child: Text('Time-Out', style: TextStyle(color: Colors.white),),
                  color: Colors.red[900],
                  onPressed: () {},
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

