import 'package:flutter/material.dart';
import 'package:mobile_ess/present/systemUI/clock_entry.dart';
import 'package:mobile_ess/present/systemUI/drawer/drawer.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final List<Widget> _children = [
      ClockEntry(),
  ];

  void onTappedBar(int index){
    setState(() {
      _currentIndex = index;
    });
  }

    int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(10),
              child: Column(
          children: <Widget>[
            // Text('Hi ${persnonnelName}', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),),
          ],
        ),
      ),
      // body: _children[_currentIndex],
      appBar: AppBar(
        title: Text('Mobile ESS'),
        backgroundColor: Colors.red[900],
      ),
      drawer: MobileDrawer(),
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTappedBar,
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        fixedColor: Colors.white,
        backgroundColor: Colors.red[900],
        unselectedItemColor: Colors.white,
        items: [
        BottomNavigationBarItem(
          icon: ImageIcon(
            AssetImage('images/ic_clock_32.png'),
          ),
          title: Text('In/Out'),
          backgroundColor: Colors.red[900],
          // activeIcon: ClockEntry()
        ),
        
         BottomNavigationBarItem(
          icon: ImageIcon(
            AssetImage('images/ic_time_32.png'),
          ),
          title: Text('Adjustment'),
          backgroundColor: Colors.red[900],
        ),
         BottomNavigationBarItem(
          icon: ImageIcon(
            AssetImage('images/ic_overtime_32.png'),
          ),
          title: Text('Overtime'),
          backgroundColor: Colors.red[900],
        ),
         BottomNavigationBarItem(
          icon: ImageIcon(
            AssetImage('images/ic_leave_32.png'),
          ),
          title: Text('Leave'),
          backgroundColor: Colors.red[900],
        ),
        
      ],
      
      // onTap: (index) {
      //   setState(() {
      //     _currentIndex = index;
      //   });
      // },
      ),
      
    );
  }
}
