import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mobile_ess/present/logic/date_drop_down.dart';
import 'package:mobile_ess/present/systemUI/home_screen.dart';


class RegistrationScreen extends StatefulWidget {
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  //  final phoneController = TextEditingController();
  //  final birthController = TextEditingController();
  //  List<TaskModel> tasks = [];
  //  TaskModel currentTask;
  

  String labelText = 'BirthDate';
  String dateFormat;
  // This Section is for DatePicker Function
  DateTime _date = DateTime.now();

  Future<Null> _selectedDate(BuildContext context) async {
    DateTime _datePicker = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: DateTime(1950),
        lastDate: DateTime(2021),
        builder: (BuildContext context, Widget child) {
          return Theme(
              data: ThemeData(
                primaryColor: Color(0xFFC41A38),
                accentColor: Color(0xFFC41A38),
              ),
              child: child);
        });
    if (_datePicker != null && _datePicker != _date) {
      setState(() {
        _date = _datePicker;
        // print(_date.toString());
        dateFormat = DateFormat.yMd().format(_date);
      });
    }
  }

  // DeviceTable currentTask;
  // final TodoHelper _todoHelper = TodoHelper();
  // List<DeviceTable> tasks = [];

  //  @override
  // void initState() {
  //   super.initState();
  //   _createProgressDialog();
  //   _firebaseMessaging.configure(
  //     onMessage: (Map<String, dynamic> message) async {
  //       print("onMessage: $message");
  //     },
  //     onLaunch: (Map<String, dynamic> message) async {
  //       print("onLaunch: $message");
  //     },
  //     onResume: (Map<String, dynamic> message) async {
  //       print("onResume: $message");
  //     },
  //   );
  //   _firebaseMessaging.getToken().then((String token) {
  //     assert(token != null);
  //     setState(() {
  //       _token = token;
  //       print("Token $token");
  //     });
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Create Account'),
        backgroundColor: Colors.red[900],
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(20, 100, 20, 15),
        child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.phone),
              title: TextField(
                keyboardType: TextInputType.phone,
                decoration: InputDecoration(
                  labelText: 'Phone Number',
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            ListTile(
              leading: Icon(Icons.date_range),
              title: DateDropDown(
                // birthController: birthController,
                labelText: labelText,
                valueText: new DateFormat.yMd().format(_date),
                onPressed: () {
                  setState(() {
                    _selectedDate(context);
                  });
                },
              ),
            ),
            SizedBox(
              height: 30,
            ),
            FlatButton(
              padding: EdgeInsets.all(15),
              child: Text(
                'Create Account',
                style: TextStyle(color: Colors.white),
              ),
              color: Colors.red[900],
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text('Create Account'),
                      content: Text('Successfully Created'),
                      actions: <Widget>[
                        FlatButton(
                          child: Text('OK'),
                          onPressed: (){
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomeScreen()));
                          },
                        ),
                      ],
                    );
                  },
                );
              },
            ),
            SizedBox(
              height: 30,
            ),

            // RaisedButton(
            //   padding: EdgeInsets.all(15),
            //   child: Text(
            //     'Create Account',
            //     style: TextStyle(color: Colors.white),
            //   ),
            //   color: Colors.red[900],
            //   onPressed: () {
            //     currentTask = DeviceTable(
            //       mobileNumber: phoneController.text,
            //       birthDate: birthController.text
            //     );
            //     _todoHelper.insertTask(currentTask);
            // Navigator.of(context).push(MaterialPageRoute(
            //   builder: (context) => HomeScreen(),
            // ));
            //   },
            // ),
            // RaisedButton(
            //   child: Text('View'),
            //   onPressed: () async {
            //     List<DeviceTable> list = await _todoHelper.getAllTask();
            //     setState(() {
            //       tasks = list;
            //     });
            //   },
            //   color: Colors.blue,
            //   textColor: Colors.white,
            // ),
            // Expanded(
            //   child: ListView.separated(
            //       itemBuilder: (context, index) {
            //         return ListTile(
            //           leading: Text("${tasks[index].personnelID}"),
            //           title: Text("${tasks[index].mobileNumber}"),
            //         );
            //       },
            //       separatorBuilder: (context, index) => Divider(),
            //       itemCount: tasks.length),
            // )
          ],
        ),
      ),
    );
  }
}
