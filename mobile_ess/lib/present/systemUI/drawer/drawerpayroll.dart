import 'package:flutter/material.dart';

class DrawerPayroll extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30.0, 0, 8.0, 20.0),
      child: InkWell(
        splashColor: Colors.red[900],
        onTap: () {},
        child: Container(
          height: 30,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              ImageIcon(AssetImage('images/ic_payslip.png')),
              Padding(padding: EdgeInsets.fromLTRB(20.0, 5.0, 5.0, 5.0),
              child: Text('Payslip'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
