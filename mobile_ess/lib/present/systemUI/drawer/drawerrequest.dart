import 'package:flutter/material.dart';

class DrawerRequest extends StatelessWidget {

  final String text;
  final ImageIcon image;
  final Function onTap;

  DrawerRequest(this.text, this.image, this.onTap);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30.0, 0, 8.0, 15.0),
      child: InkWell(
        splashColor: Colors.red[900],
        onTap: () {},
        child: Container(
          height: 30,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              image,
              Padding(padding: EdgeInsets.fromLTRB(20.0, 5.0, 5.0, 5.0),
              child: Text(text),
              )
            ],
          ),
        ),
      ),
    );
  }
}