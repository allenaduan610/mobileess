import 'package:flutter/material.dart';
import 'package:mobile_ess/present/systemUI/clock_entry.dart';
import '../drawer/drawerpayroll.dart';
import '../drawer/drawerrequest.dart';
import '../drawer/drawerclock.dart';

void main() => runApp(MobileDrawer());

class MobileDrawer extends StatelessWidget {
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            padding: EdgeInsets.fromLTRB(15, 110, 0, 10),
            child: Text(
              'Allen Christopher',
              style: TextStyle(
                color: Colors.black,
                fontSize: 16,
              ),
            ),
            decoration: BoxDecoration(
              gradient: LinearGradient(colors: <Color>[
                Colors.red,
                Colors.redAccent,
              ]),
            ),
          ),
          ListTitle('Clock Entries'),
          DrawerClock(),
          DrawerBorder(),
          ListTitle('Request/Approve'),
          DrawerRequest('Time Adjustment',
              ImageIcon(AssetImage('images/ic_time_32.png')), () {Navigator.of(context).push(MaterialPageRoute(builder: (context) => ClockEntry(),));}),
          DrawerRequest('Leave',
              ImageIcon(AssetImage('images/ic_leave_32.png')), () => {}),
          DrawerRequest('Overtime',
              ImageIcon(AssetImage('images/ic_overtime_32.png')), () => {}),
          DrawerRequest(
              'Company Loan', ImageIcon(AssetImage('images/ic_overtime_32.png')), () => {}),
          DrawerRequest('Employment Certificate',
              ImageIcon(AssetImage('images/ic_certificate.png')), () => {}),
          DrawerRequest('Viewing and Cancellation',
              ImageIcon(AssetImage('images/ic_overtime_32.png')), () => {}),
          DrawerRequest('Temporary Work Schedule',
              ImageIcon(AssetImage('images/ic_tempsched_32.png')), () => {}),
          DrawerBorder(),
          ListTitle('Payroll'),
          DrawerPayroll(),
        ],
      ),
    );
  }
}

class DrawerBorder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: Colors.grey.shade300),
        ),
      ),
    );
  }
}

class ListTitle extends StatelessWidget {
  final String text;

  ListTitle(this.text);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        text,
        style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
      ),
    );
  }
}
