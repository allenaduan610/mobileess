import 'package:flutter/material.dart';

class DateDropDown extends StatelessWidget {
  final String labelText;
  final String valueText;
  final TextEditingController birthController;
  final VoidCallback onPressed;
  final Widget child;

  const DateDropDown(
      {Key key,
      this.labelText,
      this.valueText,
      this.birthController,
      this.onPressed,
      this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 10, 10),
          child: InkWell(
        onTap: onPressed,
        child: InputDecorator(
          decoration: InputDecoration(labelText: labelText),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(valueText),
              Icon(
                Icons.arrow_drop_down,
                color: Theme.of(context).brightness == Brightness.light
                    ? Colors.grey.shade700
                    : Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }
}
