    import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class Notifications_ extends StatefulWidget {
  @override
  _Notifications_State createState() => _Notifications_State();
}

class _Notifications_State extends State<Notifications_> {

  final FirebaseMessaging _messaging=FirebaseMessaging();

  @override
  void initState() {
    super.initState();

    _messaging.getToken().then((token)
    {
      print(token);
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('notification'),
      ),
      body: Text('notification testing'),
    );
  }
}